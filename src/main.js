import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap'
import jQuery from 'jquery'
import BootstrapVue from 'bootstrap-vue'
window.$ = window.jQuery = jQuery
Vue.use(BootstrapVue)
Vue.use(BootstrapVue)
Vue.config.productionTip = false
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.prototype.axios = axios;
Vue.use(VueAxios,axios)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
